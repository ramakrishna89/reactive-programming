package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;

public class Lecture03FluxFromArrayOrList {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("a", "b", "c");

        Flux.fromIterable(list)
                .subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());

        Integer[] array = {1, 2, 3, 4, 5};

        Flux.fromArray(array)
                .subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());


    }
}
