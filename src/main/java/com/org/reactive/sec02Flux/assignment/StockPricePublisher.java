package com.org.reactive.sec02Flux.assignment;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

public class StockPricePublisher {

    public static Flux<Integer> getPrice() {
        AtomicInteger atomicInteger = new AtomicInteger(100);
        return Flux.interval(Duration.ofSeconds(1))
                .map(seconds -> atomicInteger.getAndAccumulate(
                        Utils.faker.random().nextInt(-5, 5),
                        Integer::sum
                ));
    }
}
