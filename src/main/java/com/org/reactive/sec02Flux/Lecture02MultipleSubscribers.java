package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lecture02MultipleSubscribers {

    public static void main(String[] args) {

        Flux<Integer> flux = Flux.just(1, 2, 3, 4);

        Flux<Integer> evenFlux = flux.filter(value -> value % 2 == 0);

        flux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());

        evenFlux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }
}
