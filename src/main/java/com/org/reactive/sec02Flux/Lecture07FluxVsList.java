package com.org.reactive.sec02Flux;

import com.org.reactive.sec02Flux.helper.NameService;
import com.org.reactive.util.Utils;

import java.util.List;

public class Lecture07FluxVsList {

    public static void main(String[] args) {


        //List<String> list = NameService.listNames(5);
        //System.out.println(list);

        NameService.listNamesAsFlux(5).subscribe(Utils.onNext());
    }
}
