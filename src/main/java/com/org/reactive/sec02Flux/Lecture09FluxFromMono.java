package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Lecture09FluxFromMono {

    public static void main(String[] args) {
        /*Mono<String> mono = Mono.just("Ramakrishna");

        Flux<String> flux = Flux.from(mono);
        flux.subscribe(Utils.onNext());*/

        Flux.range(1, 10)
                .filter(i -> i > 3)
                .next() // will give only first object
                .subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }
}
