package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lecture05FluxRange {

    public static void main(String[] args) {


        Flux.range(1, 10)
                .log()
                .map(i -> Utils.faker.name().fullName())
                .subscribe(Utils.onNext());
    }
}
