package com.org.reactive.sec02Flux.helper;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

public class NameService {

    public static List<String> listNames(int count) {
        List<String> list = new ArrayList<String>(count);
        for (int i = 0; i < count; i++) {
            list.add(getName());
        }
        return list;
    }

    public static Flux<String> listNamesAsFlux(int count) {
        return Flux.range(0, count)
                .map((i -> getName()));
    }

    private static String getName() {
        Utils.sleep(1);
        return Utils.faker.name().fullName();
    }
}
