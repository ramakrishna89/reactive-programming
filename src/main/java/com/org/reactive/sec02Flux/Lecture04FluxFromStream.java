package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Stream;

public class Lecture04FluxFromStream {

    public static void main(String[] args) {

        List<Integer> integerList = List.of(1, 2, 3, 4, 5);

        Stream<Integer> stream = integerList.stream();

        //stream.forEach(System.out::println);
        //stream.forEach(System.out::println); // stream will be closed at this point

        /*Flux<Integer> integerFlux = Flux.fromStream(stream);

        integerFlux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
        integerFlux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete()); // stream will be closed at this point*/

        Flux<Integer> integerFlux = Flux.fromStream(() -> integerList.stream());

        integerFlux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
        integerFlux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }
}
