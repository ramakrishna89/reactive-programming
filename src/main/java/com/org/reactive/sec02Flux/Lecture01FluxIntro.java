package com.org.reactive.sec02Flux;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lecture01FluxIntro {


    public static void main(String[] args) {

        Flux<Integer> flux = Flux.just(1, 2, 3, 4, 5);

        flux.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }
}
