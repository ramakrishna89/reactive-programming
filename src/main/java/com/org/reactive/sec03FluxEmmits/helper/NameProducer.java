package com.org.reactive.sec03FluxEmmits.helper;

import com.org.reactive.util.Utils;
import reactor.core.publisher.FluxSink;

import java.util.function.Consumer;

public class NameProducer implements Consumer<FluxSink<Object>> {

    private FluxSink<Object> fluxSink;

    @Override
    public void accept(FluxSink<Object> fluxSink) {
        this.fluxSink = fluxSink;
    }

    public void produce() {
        this.fluxSink.next(Thread.currentThread().getName() + " : " + Utils.faker.name().fullName());
    }
}
