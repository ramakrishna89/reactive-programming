package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec03FluxTake {

    public static void main(String[] args) {


        Flux.range(0, 10)
                .log()
                .take(3) // after 3 this will cancel the subscription
                .log()
                .subscribe(Utils.getDefaultSubscriber("SUB01"));
    }
}
