package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec01FluxCreate {

    public static void main(String[] args) {


        Flux.create(fluxSink -> {

            String country;
            do {
                country = Utils.faker.country().name();
                fluxSink.next(country);
            } while (!country.equalsIgnoreCase("india"));
            fluxSink.complete();

        }).subscribe(Utils.getDefaultSubscriber("SUB01"));
    }
}
