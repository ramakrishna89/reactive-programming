package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec04FluxCreateCancelCheck {

    public static void main(String[] args) {
        Flux.create(fluxSink -> {

            String country;
            do {
                country = Utils.faker.country().name();
                System.out.println("Emitting: " + country);
                fluxSink.next(country);
            } while (!country.equalsIgnoreCase("india") && !fluxSink.isCancelled());
            fluxSink.complete();

        }).take(3)
                .subscribe(Utils.getDefaultSubscriber("SUB01"));
    }
}
