package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec06FluxGenerateCounter {

    public static void main(String[] args) {

        Flux.generate(() -> 1, (counter, synchronousSink) -> {
            String country = Utils.faker.country().name();
            System.out.println("Emitting: [" + counter + "] : " + country);
            synchronousSink.next(country);
            if (country.equalsIgnoreCase("india"))
                synchronousSink.complete();
            return counter + 1;
        }).subscribe(Utils.getDefaultSubscriber());
    }
}
