package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec05FluxGenerate {

    public static void main(String[] args) {


        Flux.generate(synchronousSink -> {
            synchronousSink.next(Utils.faker.name().fullName());
            // synchronousSink.next(2); // error only one next is allowed
            synchronousSink.complete();
        }).take(3).subscribe(Utils.getDefaultSubscriber());
    }
}
