package com.org.reactive.sec03FluxEmmits;

import com.org.reactive.sec03FluxEmmits.helper.NameProducer;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec07FluxPush {

    public static void main(String[] args) {
        NameProducer producer = new NameProducer();

        Flux.push(producer).subscribe(Utils.getDefaultSubscriber("SUB01"));
        // producer.produce();

        Runnable runnable = () -> producer.produce();

        for (int i = 0; i < 10; i++) {
            new Thread(runnable).start();
        }
    }
}
