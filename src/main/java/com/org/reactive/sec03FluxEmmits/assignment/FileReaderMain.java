package com.org.reactive.sec03FluxEmmits.assignment;

import com.org.reactive.util.Utils;

public class FileReaderMain {

    public static void main(String[] args) {

        FileReaderService f = new FileReaderService();
        f.read(Utils.path.resolve("file3.txt"))
                //.map(value -> 100/0) // to check on exception file getting closed or not.
                .subscribe(Utils.getDefaultSubscriber());
    }
}
