package com.org.reactive.sec03FluxEmmits.assignment;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class FluxGenerate {

    public static void main(String[] args) {

        Flux.generate(synchronousSink -> {
            String country = Utils.faker.country().name();
            synchronousSink.next(country);
            if (country.equalsIgnoreCase("india")) {
                synchronousSink.complete();
            }
        }).subscribe(Utils.getDefaultSubscriber());

    }
}
