package com.org.reactive.sec04Operators;

import com.org.reactive.sec04Operators.helper.OrderService;
import com.org.reactive.sec04Operators.helper.UserService;
import com.org.reactive.util.Utils;

public class Lec11FlatMap {

    public static void main(String[] args) {

        UserService.getUsers()
                //.map(user -> OrderService.getOrders(user.getUserId()))
                // .flatMap(user -> OrderService.getOrders(user.getUserId()))
                .flatMap(user -> OrderService.getOrders(user.getUserId()))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);


    }
}
