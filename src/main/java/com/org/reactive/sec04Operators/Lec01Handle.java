package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec01Handle {

    public static void main(String[] args) {


        Flux.range(1, 20)
                .handle((value, sink) -> { // Handle is like a filter + map
                    if (value % 2 == 0)
                        sink.next(value);
                    else if (value == 11)
                        sink.complete();
                    else
                        sink.next(value + "a");
                }).subscribe(Utils.getDefaultSubscriber());
    }
}
