package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec04Delay {

    public static void main(String[] args) {

        Flux.range(1, 100) // get request for only 32 items
                .log()
                .delayElements(Duration.ofSeconds(1))
                .subscribe(Utils.getDefaultSubscriber());
    }
}
