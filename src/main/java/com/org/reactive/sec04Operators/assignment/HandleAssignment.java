package com.org.reactive.sec04Operators.assignment;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class HandleAssignment {


    public static void main(String[] args) {
        Flux.generate(synchronousSink -> synchronousSink.next(Utils.faker.country().name()))
                .map(Object::toString)
                .handle((value, sink) -> {
                    if (value.equalsIgnoreCase("India"))
                        sink.complete();
                    else
                        sink.next(value);
                }).subscribe(Utils.getDefaultSubscriber());
    }
}
