package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec07DefaultIfEmpty {

    public static void main(String[] args) {


        getOrderNumber()
                .filter(integer -> integer > 10)
                .defaultIfEmpty(0)
                .subscribe(Utils.getDefaultSubscriber());


    }

    private static Flux<Integer> getOrderNumber() {
        return Flux.range(1, 10);
    }
}
