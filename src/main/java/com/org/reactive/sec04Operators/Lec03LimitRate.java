package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec03LimitRate {

    public static void main(String[] args) {


        Flux.range(1, 1000)
                .log()
                // .limitRate(100) // 75% request next items
                // .limitRate(100, 99)
                // .limitRate(100, 100) // is 75%
                .limitRate(100, 0) // after 100% items request next 100%
                .subscribe(Utils.getDefaultSubscriber());
    }
}
