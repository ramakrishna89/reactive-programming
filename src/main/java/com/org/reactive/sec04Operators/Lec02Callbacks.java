package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec02Callbacks {


    public static void main(String[] args) {


        Flux.create(fluxSink -> {
            System.out.println("Emit Started...");
            for (int i = 0; i < 10; i++) {
                fluxSink.next(i);
            }
            fluxSink.complete();
            // fluxSink.error(new RuntimeException("Error on purpose....")); // Uncomment to check error lifecycle
            System.out.println("Emit Completed...");
        }).doOnComplete(() -> System.out.println("Do on Complete method...."))
                .doFirst(() -> System.out.println("Do First method.... 1"))
                .doOnNext((object) -> {
                    System.out.println("Do on Next Method: " + object);
                })
                .doOnSubscribe((subscription) -> System.out.println("Do On Subscribe method.... 1 - " + subscription))
                .doOnRequest((object) -> System.out.println("Do On Request method...." + object))
                .doOnError((error) -> System.out.println("Do On Error method...." + error))
                // .doFirst(() -> System.out.println("Do First method.... 2")) // Uncomment to check dofirst lifecycle
                // .doOnSubscribe((subscription) -> System.out.println("Do On Subscribe method.... 2 - " + subscription)) // Uncomment to check doOnSubscribe lifecycle
                .doOnTerminate(() -> System.out.println("Do On Terminate method...."))
                .doOnCancel(() -> System.out.println("Do On Cancel method...."))
                .doFinally((signalType -> System.out.println("Do On Finally method...." + signalType)))
                .doOnDiscard(Object.class, (object) -> System.out.println("Do On Discard method...." + object))
                // .doFirst(() -> System.out.println("Do First method.... 3")) // Uncomment to check dofirst lifecycle
                // .take(2) // uncomment to check cancel lifecycle
                // .doFinally((signalType -> System.out.println("Do On Finally method...." + signalType))) //Uncomment take and this to understand do finally on error / success cases
                .subscribe(Utils.getDefaultSubscriber());
    }
}
