package com.org.reactive.sec04Operators;

import com.org.reactive.sec04Operators.helper.Person;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.Locale;
import java.util.function.Function;

public class Lec10SwitchOnFirst {

    public static void main(String[] args) {


        getPerson()
                // .transform(applyPersonGt18FilterMap())
                .switchOnFirst((signal, personFlux) -> {
                    System.out.println("In switchOnFirst...");
                    return (signal.isOnNext() && signal.get().getAge() > 18) ? personFlux :
                            applyPersonGt18FilterMap().apply(personFlux);
                })
                .subscribe(Utils.getDefaultSubscriber());
    }

    private static Flux<Person> getPerson() {
        return Flux.range(1, 10)
                .map(i -> new Person());
    }

    private static Function<Flux<Person>, Flux<Person>> applyPersonGt18FilterMap() {
        return personFlux -> personFlux
                .filter(person -> person.getAge() > 15)
                .doOnNext(person -> person.setName(person.getName().toUpperCase(Locale.ROOT)))
                .doOnDiscard(Person.class, person -> System.out.println("Discarded: " + person));
    }
}
