package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Lec05OnError {

    public static void main(String[] args) {


        Flux.range(1, 10)
                .log()
                .map(i -> 10 / (5 - i))
                // .onErrorReturn(-1)
                // .onErrorResume(error -> errorhandler(error))
                .onErrorContinue((error, valueCausedError) -> System.out.println("Error: " + error + " | Value: " + valueCausedError))
                .subscribe(Utils.getDefaultSubscriber());
    }

    private static Mono<Integer> errorhandler(Throwable error) {
        // System.out.println("Existing Error: " + error);
        return Mono.fromSupplier(() -> Utils.faker.random().nextInt(100, 200));
    }
}
