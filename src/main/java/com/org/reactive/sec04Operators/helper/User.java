package com.org.reactive.sec04Operators.helper;

import com.org.reactive.util.Utils;
import lombok.Data;

@Data
public class User {

    private int userId;
    private String name;

    public User(int userId) {
        this.userId = userId;
        this.name = Utils.faker.name().fullName();
    }

}
