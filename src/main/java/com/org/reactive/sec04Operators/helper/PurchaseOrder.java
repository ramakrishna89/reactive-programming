package com.org.reactive.sec04Operators.helper;


import com.org.reactive.util.Utils;
import lombok.Data;

@Data
public class PurchaseOrder {

    private String item;
    private String price;
    private int userId;

    public PurchaseOrder(int userId) {
        this.userId = userId;
        this.item = Utils.faker.commerce().productName();
        this.price = Utils.faker.commerce().price();
    }
}
