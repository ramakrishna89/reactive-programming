package com.org.reactive.sec04Operators.helper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderService {

    private static Map<Integer, List<PurchaseOrder>> dbMap = new HashMap<>();

    static {
        List<PurchaseOrder> list1 = List.of(new PurchaseOrder(1), new PurchaseOrder(1),
                new PurchaseOrder(1));
        List<PurchaseOrder> list2 = List.of(new PurchaseOrder(2), new PurchaseOrder(2),
                new PurchaseOrder(2));
        dbMap.put(1, list1);
        dbMap.put(2, list2);
    }

    public static Flux<PurchaseOrder> getOrders(int userId) {
        return Flux.create((FluxSink<PurchaseOrder> purchaseOrderFluxSink) -> {
            dbMap.get(userId).forEach(purchaseOrderFluxSink::next);
            purchaseOrderFluxSink.complete();
        }).delayElements(Duration.ofSeconds(1));
    }
}
