package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec08SwitchIfEmpty {

    public static void main(String[] args) {


        getOrderNumber()
                // .filter(integer -> integer > 9)
                .filter(integer -> integer > 10)
                .switchIfEmpty(getGreaterthan10())
                .subscribe(Utils.getDefaultSubscriber());


    }

    private static Flux<Integer> getOrderNumber() {
        return Flux.range(1, 10);
    }

    private static Flux<Integer> getGreaterthan10() {
        return Flux.range(11, 20);
    }
}

