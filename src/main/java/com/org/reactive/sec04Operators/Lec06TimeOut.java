package com.org.reactive.sec04Operators;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec06TimeOut {

    public static void main(String[] args) {

        getOrderNumbers()
                // .timeout(Duration.ofSeconds(1))
                .timeout(Duration.ofSeconds(2), fallbackMethod())
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }

    private static Flux<Integer> getOrderNumbers() {
        return Flux.range(1, 10)
                // .delayElements(Duration.ofSeconds(5));
                .delayElements(Duration.ofSeconds(1));
    }

    private static Flux<Integer> fallbackMethod() {
        return Flux.range(1, 10)
                .delayElements(Duration.ofMillis(200));
    }
}
