package com.org.reactive.util;

import com.github.javafaker.Faker;
import org.reactivestreams.Subscriber;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;

public class Utils {

    public static final Path path = Paths.get("src/main/resources/assignment");
    public static final Faker faker = Faker.instance();

    public static Consumer<Object> onNext() {
        return o -> System.out.println("Received Data: " + o);
    }

    public static Consumer<Throwable> onError() {
        return o -> System.out.println("Error: " + o);
    }

    public static Runnable onComplete() {
        return () -> System.out.println("Completed");
    }

    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleepInMills(int mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Subscriber<Object> getDefaultSubscriber() {
        return new DefaultSubscriber();
    }

    public static Subscriber<Object> getDefaultSubscriber(String name) {
        return new DefaultSubscriber(name);
    }
}
