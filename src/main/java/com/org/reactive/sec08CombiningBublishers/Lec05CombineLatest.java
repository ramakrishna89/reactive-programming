package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec05CombineLatest {


    public static void main(String[] args) {


        Flux<String> stringFlux = Flux.combineLatest(getString(), getNumber(), (s, i) -> s + i);

        stringFlux.subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(10);

    }


    public static Flux<String> getString() {
        return Flux.just("A", "B", "C", "D")
                .delayElements(Duration.ofSeconds(1));
    }

    public static Flux<Integer> getNumber() {
        return Flux.just(1, 2, 3, 4)
                .delayElements(Duration.ofSeconds(3));
    }
}
