package com.org.reactive.sec08CombiningBublishers.helper;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class CanadaFlight {

    public static Flux<String> getFlights() {
        return Flux.range(1, Utils.faker.random().nextInt(1, 10))
                .delayElements(Duration.ofSeconds(1))
                .map(i -> "CAD" + Utils.faker.random().nextInt(100, 999))
                .filter(s -> Utils.faker.random().nextBoolean());
    }
}
