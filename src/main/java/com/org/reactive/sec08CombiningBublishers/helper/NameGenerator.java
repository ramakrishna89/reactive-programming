package com.org.reactive.sec08CombiningBublishers.helper;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

public class NameGenerator {


    private List<String> nameList = new ArrayList<>();

    public Flux<String> generateName() {
        return Flux.generate(synchronousSink -> {
            System.out.println("In Generate Name");
            Utils.sleep(1);
            String name = Utils.faker.name().fullName();
            nameList.add(name);
            synchronousSink.next(name);
        }).cast(String.class)
                .startWith(getNameFromCache());
    }

    public Flux<String> getNameFromCache() {
        return Flux.fromIterable(nameList);
    }
}
