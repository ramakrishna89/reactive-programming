package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec06Assignment {

    public static void main(String[] args) {

       /* final int inclCarPrice = 10000;
        Flux<Double> flux = Flux.combineLatest(monthStream(), demandStream(), (m, d) -> {
            return ((inclCarPrice - (m * 100)) * d);
        });

        flux.subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);*/

        Flux<String> flux = Flux.just("a", "b", "c");
        flux.startWith(flux)
                .subscribe(Utils.getDefaultSubscriber());

    }

    private static Flux<Long> monthStream() {
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(1));
    }


    private static Flux<Double> demandStream() {
        return Flux.interval(Duration.ofSeconds(3))
                .map(i -> Utils.faker.random().nextInt(80, 120) / 100d)
                .startWith(1d);
    }
}
