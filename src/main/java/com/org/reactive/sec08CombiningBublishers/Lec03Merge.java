package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.sec08CombiningBublishers.helper.CanadaFlight;
import com.org.reactive.sec08CombiningBublishers.helper.IndiaFlight;
import com.org.reactive.sec08CombiningBublishers.helper.QatarFlight;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec03Merge {


    public static void main(String[] args) {

        Flux<String> merge = Flux.merge(QatarFlight.getFlights(), IndiaFlight.getFlights(), CanadaFlight.getFlights());

        merge.subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(10);

    }
}
