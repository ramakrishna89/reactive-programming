package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple3;

public class Lec04Zip {


    public static void main(String[] args) {

        Flux<Tuple3<String, String, String>> zip = Flux.zip(getBody(), getEngine(), getTyres());
        zip.subscribe(Utils.getDefaultSubscriber());
    }


    private static Flux<String> getBody() {
        return Flux.range(1, 5)
                .map(i -> "BODY" + i);
    }

    private static Flux<String> getEngine() {
        return Flux.range(1, 2)
                .map(i -> "ENGN" + i);
    }

    private static Flux<String> getTyres() {
        return Flux.range(1, 6)
                .map(i -> "TYRE" + i);
    }
}
