package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.sec08CombiningBublishers.helper.NameGenerator;
import com.org.reactive.util.Utils;

public class Lec01StartsWith {


    public static void main(String[] args) {


        NameGenerator nameGenerator = new NameGenerator();
        nameGenerator.generateName()
                .take(2)
                .subscribe(Utils.getDefaultSubscriber("USER1"));

        nameGenerator.generateName()
                .take(2)
                .subscribe(Utils.getDefaultSubscriber("USER2"));

        nameGenerator.generateName()
                .filter(name -> name.startsWith("A"))
                .take(3)
                .subscribe(Utils.getDefaultSubscriber("USER3"));

    }
}
