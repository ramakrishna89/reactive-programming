package com.org.reactive.sec08CombiningBublishers;

import com.org.reactive.sec08CombiningBublishers.helper.NameGenerator;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec02Concat {


    public static void main(String[] args) {

        Flux<String> flux1 = Flux.just("a", "b");
        Flux<String> flux2 = Flux.error(new RuntimeException("Error"));
        Flux<String> flux3 = Flux.just("c", "d", "e");


        // Flux<String> concatFlux = flux1.concatWith(flux2);
        // Flux<String> concatFlux = Flux.concat(flux1, flux2, flux3);
        Flux<String> concatFlux = Flux.concatDelayError(flux1, flux2, flux3);

        concatFlux.subscribe(Utils.getDefaultSubscriber());


    }
}
