package com.org.reactive.sec09Batch;

import com.org.reactive.sec09Batch.helper.BookOrder;
import com.org.reactive.sec09Batch.helper.RevenueReport;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Lec03Assignment {

    public static void main(String[] args) {


        Set<String> allowedCategories = Set.of("Science fiction", "Fantasy", "Suspense/Thriller");
        bookStream().filter(bookOrder -> allowedCategories.contains(bookOrder.getCategory()))
                .buffer(Duration.ofSeconds(5))
                .map(list -> revenueCalculator(list))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);

    }

    private static RevenueReport revenueCalculator(List<BookOrder> bookOrderList) {
        Map<String, Double> map = bookOrderList.stream()
                .collect(Collectors.groupingBy(BookOrder::getCategory, Collectors.summingDouble(BookOrder::getPrice)));
        return new RevenueReport(map);
    }

    private static Flux<BookOrder> bookStream() {
        return Flux.interval(Duration.ofMillis(200))
                .map(i -> new BookOrder());
    }
}
