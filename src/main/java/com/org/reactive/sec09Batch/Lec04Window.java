package com.org.reactive.sec09Batch;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

public class Lec04Window {

    private static AtomicInteger atomicInteger = new AtomicInteger(1);

    public static void main(String[] args) {


        eventStream()
                // .window(5)
                .window(Duration.ofSeconds(2))
                .flatMap(flux -> saveEvents(flux))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }


    private static Flux<String> eventStream() {
        return Flux.interval(Duration.ofMillis(300))
                .map(i -> "EVENT:" + i);
    }

    private static Mono<Integer> saveEvents(Flux<String> flux) {
        return flux.doOnNext(s -> System.out.println("Saving event: " + s))
                .doOnComplete(() -> {
                    System.out.println("Saving batch successful");
                    System.out.println("-----------------------");
                }).then(Mono.just(atomicInteger.getAndIncrement()));
    }
}
