package com.org.reactive.sec09Batch;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec01Buffer {

    public static void main(String[] args) {


        eventStream()
                //.buffer(5)
                // .buffer(Duration.ofSeconds(2))
                .bufferTimeout(5, Duration.ofSeconds(2))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }


    private static Flux<String> eventStream() {
        return Flux.interval(Duration.ofMillis(10))
                //.take(3)
                .map(i -> "EVENT:" + i);
    }
}
