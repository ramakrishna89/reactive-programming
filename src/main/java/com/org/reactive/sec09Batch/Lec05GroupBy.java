package com.org.reactive.sec09Batch;

import com.org.reactive.util.Utils;
import jdk.jshell.execution.Util;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec05GroupBy {


    public static void main(String[] args) {


        Flux.range(1, 30)
                .delayElements(Duration.ofSeconds(1))
                .groupBy(integer -> integer % 2)
                .subscribe(groupedFlux -> process(groupedFlux, groupedFlux.key()));

        Utils.sleep(60);

    }

    private static void process(Flux<Integer> flux, int key) {
        System.out.println("In process method...");
        flux.subscribe(integer -> System.out.println("Key: " + key + " Element: " + integer));
    }
}
