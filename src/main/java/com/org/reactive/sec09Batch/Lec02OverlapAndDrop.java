package com.org.reactive.sec09Batch;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class Lec02OverlapAndDrop {

    public static void main(String[] args) {


        eventStream()
                // .buffer(3, 1)
                // .buffer(3, 3)
                .buffer(3, 4)
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }


    private static Flux<String> eventStream() {
        return Flux.interval(Duration.ofMillis(300))
                .map(i -> "EVENT:" + i);
    }
}
