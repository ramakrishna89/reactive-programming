package com.org.reactive.sec09Batch.assignment;

import com.org.reactive.util.Utils;
import lombok.Data;


@Data
public class PurchaseOrder {

    private String item;
    private double price;
    private String department;


    public PurchaseOrder() {
        this.item = Utils.faker.commerce().productName();
        this.price = Double.parseDouble(Utils.faker.commerce().price());
        this.department = Utils.faker.commerce().department();
    }
}
