package com.org.reactive.sec09Batch.assignment;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Function;

public class OrderProcessor {

    public static Function<Flux<PurchaseOrder>, Flux<PurchaseOrder>> automotiveProcessor() {
        return purchaseOrderFlux -> purchaseOrderFlux
                .doOnNext(purchaseOrder -> {
                    purchaseOrder.setPrice(1.1 * purchaseOrder.getPrice());
                    purchaseOrder.setItem("{{ " + purchaseOrder.getItem() + " }}");
                });
    }


    public static Function<Flux<PurchaseOrder>, Flux<PurchaseOrder>> kidsProcessor() {
        return purchaseOrderFlux -> purchaseOrderFlux
                .doOnNext(purchaseOrder -> {
                    purchaseOrder.setPrice(0.5 * purchaseOrder.getPrice());
                    purchaseOrder.setItem(purchaseOrder.getItem());
                })
                // .flatMap(purchaseOrder -> Flux.just(purchaseOrder, getFreePurchaseOrder()));
                .flatMap(purchaseOrder -> Flux.concat(Mono.just(purchaseOrder), getFreePurchaseOrderMono()));
    }

    private static PurchaseOrder getFreePurchaseOrder() {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setPrice(0);
        purchaseOrder.setItem("FREE-" + purchaseOrder.getItem());
        purchaseOrder.setDepartment("Kids");
        return purchaseOrder;
    }

    // another way of doing
    private static Mono<PurchaseOrder> getFreePurchaseOrderMono() {
        return Mono.fromSupplier(() -> {
            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setPrice(0);
            purchaseOrder.setItem("FREE-" + purchaseOrder.getItem());
            purchaseOrder.setDepartment("Kids");
            return purchaseOrder;
        });
    }
}
