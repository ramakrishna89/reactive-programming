package com.org.reactive.sec09Batch.helper;

import com.github.javafaker.Book;
import com.org.reactive.util.Utils;
import lombok.Data;

@Data
public class BookOrder {


    private String title;
    private String author;
    private String category;
    private double price;

    public BookOrder() {
        final Book book = Utils.faker.book();
        this.title = book.title();
        this.author = book.author();
        this.category = book.genre();
        this.price = Double.parseDouble(Utils.faker.commerce().price());
    }
}
