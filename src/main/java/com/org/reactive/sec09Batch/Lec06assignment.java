package com.org.reactive.sec09Batch;

import com.org.reactive.sec09Batch.assignment.OrderProcessor;
import com.org.reactive.sec09Batch.assignment.OrderService;
import com.org.reactive.sec09Batch.assignment.PurchaseOrder;
import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public class Lec06assignment {

    public static void main(String[] args) {


        Map<String, Function<Flux<PurchaseOrder>, Flux<PurchaseOrder>>> map = Map.of(
                "Kids", OrderProcessor.kidsProcessor(),
                "Automotive", OrderProcessor.automotiveProcessor()
        );

        Set<String> keySet = map.keySet();

        OrderService.orderStream()
                .filter(purchaseOrder -> keySet.contains(purchaseOrder.getDepartment()))
                .groupBy(PurchaseOrder::getDepartment)
                .flatMap(groupedFlux -> map.get(groupedFlux.key()).apply(groupedFlux))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }
}
