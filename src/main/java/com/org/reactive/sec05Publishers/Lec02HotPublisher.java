package com.org.reactive.sec05Publishers;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Stream;

public class Lec02HotPublisher {

    public static void main(String[] args) {


        Flux<String> flux = Flux.fromStream(() -> getMovies())
                .delayElements(Duration.ofSeconds(2))
                .share();

        flux.subscribe(Utils.getDefaultSubscriber("USER1"));

        Utils.sleep(5);

        flux.subscribe(Utils.getDefaultSubscriber("USER2"));

        Utils.sleep(60);
    }


    //Movie theator
    private static Stream<String> getMovies() {
        System.out.println("In getMovies....");
        return Stream.of(
                "Scene 1",
                "Scene 2",
                "Scene 3",
                "Scene 4",
                "Scene 5"
        );
    }
}
