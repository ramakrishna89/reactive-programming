package com.org.reactive.sec05Publishers.assignment;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Objects;

public class OrderService {

    private Flux<PurchaseOrder> purchaseOrder;

    public Flux<PurchaseOrder> getOrderStream() {
        if (Objects.isNull(purchaseOrder))
            purchaseOrder = createOrderStream();
        return purchaseOrder;
    }

    private Flux<PurchaseOrder> createOrderStream() {
        return Flux.interval(Duration.ofMillis(100))
                .map(i -> new PurchaseOrder())
                .publish()
                .refCount(2);
    }
}
