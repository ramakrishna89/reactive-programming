package com.org.reactive.sec05Publishers.assignment;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class AssignmentMain {

    public static void main(String[] args) {

        OrderService orderService = new OrderService();
        RevenueService revenueService = new RevenueService();
        InventoryService inventoryService = new InventoryService();

        orderService.getOrderStream().subscribe(revenueService.subscribeOrderStream());
        orderService.getOrderStream().subscribe(inventoryService.subscribeOrderStream());


        inventoryService.inventoryStream().subscribe(Utils.getDefaultSubscriber("USER1"));
        revenueService.revenueStream().subscribe(Utils.getDefaultSubscriber("USER2"));

        Utils.sleep(60);


    }
}
