package com.org.reactive.sec10RepeatRetry;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;

public class Lec04RetrySpec {


    //Payment Service
    private static void processPayment(String cardNumber) {
        int random = Utils.faker.random().nextInt(1, 10);
        if (random < 8)
            throw new RuntimeException("500");
        else if (random < 10)
            throw new RuntimeException("404");
    }

    //Order Service
    private static Mono<String> orderService(String cardNumber) {
        return Mono.fromSupplier(() -> {
            processPayment(cardNumber);
            return Utils.faker.idNumber().valid();
        });
    }

    public static void main(String[] args) {

        orderService(Utils.faker.business().creditCardNumber())
                .doOnError(error -> System.out.println("DoOnError: " + error))
                // .retry(5)
                .retryWhen(Retry.from(
                        retrySignalFlux -> retrySignalFlux.doOnNext(retrySignal -> {
                            System.out.println("Total Retries: " + retrySignal.totalRetries());
                            System.out.println("Signal error: " + retrySignal.failure());
                        }).handle((retrySignal, synchronousSink) -> {
                            if (retrySignal.failure().getMessage().equalsIgnoreCase("500"))
                                synchronousSink.next(1);
                            else
                                synchronousSink.error(retrySignal.failure());
                        }).delayElements(Duration.ofSeconds(1))
                ))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);

    }


}
