package com.org.reactive.sec10RepeatRetry;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

public class Lec01RepeatDemo {

    private static AtomicInteger atomicInteger = new AtomicInteger(1);

    public static void main(String[] args) {


        getIntegers()
                // .repeat(3)
                //  .repeat()
                .repeat(() -> atomicInteger.get() < 14)
                .subscribe(Utils.getDefaultSubscriber());
    }

    private static Flux<Integer> getIntegers() {
        return Flux.range(1, 3)
                .doOnSubscribe(i -> System.out.println("--SUBS"))
                .doOnComplete(() -> System.out.println("--COMP"))
                .map(integer -> atomicInteger.getAndIncrement());
        //.map(i -> i / 0);
    }

}
