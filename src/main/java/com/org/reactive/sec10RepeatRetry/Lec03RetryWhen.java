package com.org.reactive.sec10RepeatRetry;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

public class Lec03RetryWhen {

    private static AtomicInteger atomicInteger = new AtomicInteger(1);

    public static void main(String[] args) {


        getIntegers()
                .retryWhen(Retry.fixedDelay(2, Duration.ofSeconds(3)))
                .subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }

    private static Flux<Integer> getIntegers() {
        return Flux.range(1, 3)
                .doOnSubscribe(i -> System.out.println("--SUBS"))
                .doOnComplete(() -> System.out.println("--COMP"))
                .map(integer -> atomicInteger.getAndIncrement())
                .map(i -> i / (Utils.faker.random().nextInt(1, 5) > 3 ? 0 : 1))
                .doOnError(error -> System.out.println("DoOnError: " + error));
    }

}
