package com.org.reactive.sec11Sink;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.time.Duration;

public class Lec05SinkMultiDirectAll {

    public static void main(String[] args) {


        System.setProperty("reactor.bufferSize.small", "16");

        // handle to push items
        // Sinks.Many<Integer> sink = Sinks.many().multicast().directAllOrNothing();
        Sinks.Many<Integer> sink = Sinks.many().multicast().directBestEffort();


        // handle to subscribe items
        Flux<Integer> flux = sink.asFlux();

        flux.subscribe(Utils.getDefaultSubscriber("USER1"));

        flux.delayElements(Duration.ofMillis(200))
                .subscribe(Utils.getDefaultSubscriber("USER2"));

        for (int i = 0; i < 100; i++) {
            sink.tryEmitNext(i);
        }

        Utils.sleep(10);

    }
}
