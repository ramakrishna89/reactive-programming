package com.org.reactive.sec11Sink;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

public class Lec01SinkOne {

    public static void main(String[] args) {

        Sinks.One<Object> sink = Sinks.one();

        Mono<Object> mono = sink.asMono();

        mono.subscribe(Utils.getDefaultSubscriber("USER1"));
        mono.subscribe(Utils.getDefaultSubscriber("USER2"));

        // sink.tryEmitValue("Hi");
        // sink.tryEmitEmpty();
        // sink.tryEmitError(new RuntimeException("Test Error"));

       /* sink.emitValue("Hi", (signalType, emitResult) -> {
            System.out.println("signalType: " + signalType.name());
            System.out.println("emitResult: " + emitResult.name());
            return false;
        });

        // Produce error since we using mono
        sink.emitValue("Hello", (signalType, emitResult) -> {
            System.out.println("signalType: " + signalType.name());
            System.out.println("emitResult: " + emitResult.name());
            return true;
        });*/

        sink.tryEmitValue("Hi");



    }
}
