package com.org.reactive.sec11Sink.assignment;


import lombok.Data;

import java.util.function.Consumer;

@Data
public class SlackMember {

    private String name;
    private Consumer<String> messageConsumer;

    public SlackMember(String name) {
        this.name = name;
    }


    public void receiveMessage(String message) {
        System.out.println(message);
    }

    public void sendMessage(String message) {
        this.messageConsumer.accept(message);
    }
}
