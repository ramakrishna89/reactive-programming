package com.org.reactive.sec11Sink.assignment;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

public class SlackRoom {

    private String roomName;
    private Sinks.Many<SlackMessage> slackMessageSink;
    private Flux<SlackMessage> slackMessageFlux;

    public SlackRoom(String roomName) {
        this.roomName = roomName;
        this.slackMessageSink = Sinks.many().replay().all();
        this.slackMessageFlux = slackMessageSink.asFlux();
    }


    public void joinSlackRoom(SlackMember slackMember) {
        System.out.println("Welcome " + slackMember.getName() + " !!!");
        this.receiveMessage(slackMember);
        slackMember.setMessageConsumer(s -> {
            this.postMessage(s, slackMember);
        });
    }

    private void postMessage(String message, SlackMember slackMember) {
        SlackMessage slackMessage = new SlackMessage();
        slackMessage.setMessage(message);
        slackMessage.setSender(slackMember.getName());
        this.slackMessageSink.tryEmitNext(slackMessage);
    }

    private void receiveMessage(SlackMember slackMember) {
        this.slackMessageFlux
                .filter(slackMessage -> !slackMessage.getSender().equalsIgnoreCase(slackMember.getName()))
                .doOnNext(slackMessage -> slackMessage.setReceiver(slackMember.getName()))
                .map(slackMessage -> slackMessage.toString())
                .subscribe(s -> slackMember.receiveMessage(s));
    }
}
