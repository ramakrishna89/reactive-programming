package com.org.reactive.sec11Sink;

import com.org.reactive.sec11Sink.assignment.SlackMember;
import com.org.reactive.sec11Sink.assignment.SlackRoom;
import com.org.reactive.util.Utils;

public class Lec07SlackAssignment {


    public static void main(String[] args) {

        SlackRoom reactorRoom = new SlackRoom("REACTOR ROOM");

        SlackMember user1 = new SlackMember("USER1");
        SlackMember user2 = new SlackMember("USER2");
        SlackMember user3 = new SlackMember("USER3");

        reactorRoom.joinSlackRoom(user1);
        reactorRoom.joinSlackRoom(user2);

        user1.sendMessage("Hi All");

        Utils.sleep(2);

        user2.sendMessage("Hi User 1");

        Utils.sleep(2);

        reactorRoom.joinSlackRoom(user3);

        user2.sendMessage("Hi Users !!!!! asdasdasd");

        Utils.sleep(2);

    }

}
