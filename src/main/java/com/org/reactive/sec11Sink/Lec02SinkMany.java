package com.org.reactive.sec11Sink;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

public class Lec02SinkMany {

    public static void main(String[] args) {


        // handle to push items
        Sinks.Many<String> sink = Sinks.many().unicast().onBackpressureBuffer();


        // handle to subscribe items
        Flux<String> flux = sink.asFlux();

        flux.subscribe(Utils.getDefaultSubscriber("USER1"));
        flux.subscribe(Utils.getDefaultSubscriber("USER2"));

        sink.tryEmitNext("Hi");
        sink.tryEmitNext("how are you");
        sink.tryEmitNext("?");

    }
}
