package com.org.reactive.sec11Sink;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

public class Lec06Reply {

    public static void main(String[] args) {


        // handle to push items
        Sinks.Many<String> sink = Sinks.many().replay().all();
        // Sinks.Many<String> sink = Sinks.many().multicast().directAllOrNothing();


        // handle to subscribe items
        Flux<String> flux = sink.asFlux();

        sink.tryEmitNext("MSG 1");
        sink.tryEmitNext("MSG 2");
        flux.subscribe(Utils.getDefaultSubscriber("USER1"));

        sink.tryEmitNext("Hi");
        sink.tryEmitNext("how are you");

        flux.subscribe(Utils.getDefaultSubscriber("USER2"));

        sink.tryEmitNext("?");

    }
}
