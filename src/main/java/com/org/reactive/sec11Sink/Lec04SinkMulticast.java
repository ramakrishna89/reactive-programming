package com.org.reactive.sec11Sink;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Lec04SinkMulticast {

    public static void main(String[] args) {


        // handle to push items
        Sinks.Many<String> sink = Sinks.many().multicast().onBackpressureBuffer();
        // Sinks.Many<String> sink = Sinks.many().multicast().directAllOrNothing();


        // handle to subscribe items
        Flux<String> flux = sink.asFlux();

        sink.tryEmitNext("MSG 1");
        sink.tryEmitNext("MSG 2");
        flux.subscribe(Utils.getDefaultSubscriber("USER1"));

        sink.tryEmitNext("Hi");
        sink.tryEmitNext("how are you");

        flux.subscribe(Utils.getDefaultSubscriber("USER2"));

        sink.tryEmitNext("?");

    }
}
