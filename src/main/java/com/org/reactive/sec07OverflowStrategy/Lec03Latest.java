package com.org.reactive.sec07OverflowStrategy;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class Lec03Latest {

    public static void main(String[] args) {

        System.setProperty("reactor.bufferSize.small", "16");

        Flux.create(fluxSink -> {
            for (int i = 0; i < 201; i++) {
                fluxSink.next(i);
                System.out.println("Emitted: " + i);
                //Utils.sleepInMills(1);
            }
            fluxSink.complete();
        })
                .onBackpressureLatest()
                .publishOn(Schedulers.boundedElastic())
                .doOnNext(i -> {
                    Utils.sleepInMills(10);
                }).subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(5);
    }
}
