package com.org.reactive.sec07OverflowStrategy;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.util.concurrent.Queues;

import java.util.ArrayList;
import java.util.List;

public class Lec02Drop {

    public static void main(String[] args) {

        System.setProperty("reactor.bufferSize.small", "16");

        List<Object> droppedValues = new ArrayList();

        Flux.create(fluxSink -> {
            for (int i = 0; i < 501; i++) {
                fluxSink.next(i);
                System.out.println("Emitted: " + i);
                Utils.sleepInMills(1);
            }
            fluxSink.complete();
        })
                //.onBackpressureDrop()
                .onBackpressureDrop(i -> droppedValues.add(i))
                .publishOn(Schedulers.boundedElastic())
                .doOnNext(i -> {
                    Utils.sleepInMills(10);
                }).subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(5);
        System.out.println(droppedValues);
    }
}
