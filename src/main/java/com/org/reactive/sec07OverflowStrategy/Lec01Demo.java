package com.org.reactive.sec07OverflowStrategy;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class Lec01Demo {

    public static void main(String[] args) {


        Flux.create(fluxSink -> {
            for (int i = 0; i < 501; i++) {
                fluxSink.next(i);
                System.out.println("Emitted: " + i);
            }
            fluxSink.complete();
        }).publishOn(Schedulers.boundedElastic())
                .doOnNext(i -> {
                    Utils.sleepInMills(10);
                }).subscribe(Utils.getDefaultSubscriber());

        Utils.sleep(60);
    }
}
