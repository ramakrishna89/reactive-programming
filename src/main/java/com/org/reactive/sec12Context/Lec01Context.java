package com.org.reactive.sec12Context;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.Locale;

public class Lec01Context {

    public static void main(String[] args) {


        getWelcome()
                .contextWrite(context -> context.put("user", context.get("USER").toString().toLowerCase(Locale.ROOT)))
                .contextWrite(Context.of("USER", "RAM")) // immutabale key USER value dhivya removed
                .contextWrite(Context.of("USER", "DHIVYA"))
                .contextWrite(Context.of("USERS", "RAMAKRISHNA"))
                .subscribe(Utils.getDefaultSubscriber());
    }

    private static Mono<String> getWelcome() {
        return Mono.deferContextual(contextView -> {
            if (contextView.hasKey("user"))
                return Mono.just("Welcome " + contextView.get("user"));
            else
                return Mono.error(new RuntimeException("Not Authenticated"));
        });
    }
}
