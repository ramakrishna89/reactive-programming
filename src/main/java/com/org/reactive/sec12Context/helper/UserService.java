package com.org.reactive.sec12Context.helper;

import reactor.util.context.Context;

import java.util.Map;
import java.util.function.Function;

public class UserService {

    private static final Map<String, String> map = Map.of(
            "USER1", "STD",
            "USER2", "PRIME"
    );

    public static Function<Context, Context> userCategoryContext() {
        return context -> {
            String user = context.get("USER");
            String category = map.get(user);
            return context.put("CATEGORY", category);
        };
    }
}
