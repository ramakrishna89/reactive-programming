package com.org.reactive.sec12Context.helper;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class BookService {

    private static Map<String, Integer> map = new HashMap<>();

    static {
        map.put("STD", 2);
        map.put("PRIME", 3);
    }


    public static Mono<String> getBook() {
        return Mono.deferContextual(context -> {
            if (context.get("ALLOW"))
                return Mono.just(Utils.faker.book().title());
            else
                return Mono.error(new RuntimeException("Not Allowed"));
        }).contextWrite(rateLimiterContext());
    }


    private static Function<Context, Context> rateLimiterContext() {
        return context -> {
            if (context.hasKey("CATEGORY")) {
                String category = context.get("CATEGORY").toString();
                Integer attempts = map.get(category);
                if (attempts > 0) {
                    map.put(category, attempts - 1);
                    return context.put("ALLOW", true);
                }
            }
            return context.put("ALLOW", false);
        };
    }
}
