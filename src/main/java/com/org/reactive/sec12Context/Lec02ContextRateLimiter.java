package com.org.reactive.sec12Context;

import com.org.reactive.sec12Context.helper.BookService;
import com.org.reactive.sec12Context.helper.UserService;
import com.org.reactive.util.Utils;
import reactor.util.context.Context;

public class Lec02ContextRateLimiter {

    public static void main(String[] args) {


        BookService.getBook()
                .repeat(4)
                .contextWrite(UserService.userCategoryContext())
                .contextWrite(Context.of("USER", "USER2"))
                .subscribe(Utils.getDefaultSubscriber());
    }
}
