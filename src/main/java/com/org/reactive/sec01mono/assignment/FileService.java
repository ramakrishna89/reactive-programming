package com.org.reactive.sec01mono.assignment;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;

public class FileService {

    public static Mono<String> read(String fileName) {
        return Mono.fromSupplier(() -> readFile(fileName));
    }

    private static String readFile(String fileName) {
        try {
            return Files.readString(Utils.path.resolve(fileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Mono<Void> write(String fileName, String content) {
        return Mono.fromRunnable(() -> writeFile(fileName, content));
    }

    private static void writeFile(String fileName, String content) {
        try (BufferedWriter bw = Files.newBufferedWriter(Utils.path.resolve(fileName))) {
            for (int i = 0; i < 10000; i++) {
                bw.write(content + i);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Mono<Void> delete(String fileName) {
        return Mono.fromRunnable(() -> deleteFile(fileName));
    }

    private static void deleteFile(String fileName) {
        try {
            Files.delete(Utils.path.resolve(fileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
