package com.org.reactive.sec01mono.assignment;

import com.org.reactive.util.Utils;

public class MonoFileProcessor {

    public static void main(String[] args) {

       /* FileService.read("file3.txt")
                .subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());*/

        FileService.write("file3.txt", "This is line ")
                .subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());

        // FileService.delete("file3.txt").subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());

    }
}
