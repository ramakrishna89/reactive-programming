package com.org.reactive.sec01mono;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class Lecture06SupplierRefactoring {

    public static void main(String[] args) {
        getName();
        //getName().subscribe(Utils.onNext());
        getName()
                .subscribeOn(Schedulers.boundedElastic())
                .subscribe(Utils.onNext());
        getName();
    }

    private static Mono<String> getName() {
        System.out.println("In getName method");
        return Mono.fromSupplier(() -> {
            System.out.println("Operation in progress....");
            Utils.sleep(5);
            return Utils.faker.name().fullName();
        }).map(String::toUpperCase);
    }
}
