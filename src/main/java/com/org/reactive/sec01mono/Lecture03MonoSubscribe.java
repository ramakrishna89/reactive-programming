package com.org.reactive.sec01mono;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;

public class Lecture03MonoSubscribe {

    public static void main(String[] args) {
        // Mono<String> mono = Mono.just("Ramakrishna");
        Mono<Integer> mono = Mono.just("Ramakrishna")
                .map(String::length)
                .map(value -> value / 0);
        /*mono.subscribe(
                response -> System.out.println(response),
                error -> System.out.println("Error in subscribe: " + error),
                () -> System.out.println("Application completed")
        );*/

        /*mono.subscribe(
                response -> System.out.println(response)
        );*/

        mono.subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }
}
