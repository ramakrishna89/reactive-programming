package com.org.reactive.sec01mono;

import reactor.core.publisher.Mono;

public class Lecture02Mono {

    public static void main(String[] args) {
        Mono<Integer> mono = Mono.just(1);
        System.out.println(mono);

        mono.subscribe(i -> System.out.println("Received: " + i));
    }
}
