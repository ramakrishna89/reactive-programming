package com.org.reactive.sec01mono;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

public class Lecture05MonoFromSupplier {


    public static void main(String[] args) {

        // use just only for hardcoded data
        // Mono<String> mono = Mono.just(getName());

        /*Supplier<String> getNameSupplier = () -> getName();
        Mono<String> monoSupplierEg = Mono.fromSupplier(getNameSupplier);
        monoSupplierEg.subscribe(Utils.onNext());*/

        Callable<String> getNameCallable = () -> getName();
        Mono<String> monoCallableEg = Mono.fromCallable(getNameCallable);
        monoCallableEg.subscribe(Utils.onNext());

    }

    private static String getName() {
        System.out.println("Operation in progress....");
        return Utils.faker.name().fullName();
    }
}
