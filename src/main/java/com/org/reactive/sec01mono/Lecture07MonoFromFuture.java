package com.org.reactive.sec01mono;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

public class Lecture07MonoFromFuture {

    public static void main(String[] args) {

        Mono.fromFuture(getName()).subscribe(Utils.onNext());
        // Optional only to see output
        // Utils.sleep(1);
    }

    private static CompletableFuture<String> getName() {
        return CompletableFuture.supplyAsync(() -> Utils.faker.name().fullName());
    }
}
