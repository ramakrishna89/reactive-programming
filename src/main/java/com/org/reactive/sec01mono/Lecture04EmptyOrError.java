package com.org.reactive.sec01mono;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Mono;

public class Lecture04EmptyOrError {

    public static void main(String[] args) {
        getNameFromRepository(3).subscribe(Utils.onNext(), Utils.onError(), Utils.onComplete());
    }

    private static Mono<String> getNameFromRepository(int userId) {
        if (userId == 1) {
            return Mono.just(Utils.faker.name().fullName());
        } else if (userId == 2) {
            return Mono.empty();
        } else {
            return Mono.error(new RuntimeException("Fabricated Error"));
        }
    }
}
