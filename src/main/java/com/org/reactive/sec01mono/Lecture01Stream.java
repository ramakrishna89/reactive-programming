package com.org.reactive.sec01mono;

import java.util.stream.Stream;

public class Lecture01Stream {

    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.of(1)
                .map(i -> {
                    try {
                        Thread.sleep(1 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return i * 2;
                });
        integerStream.forEach(System.out::println);
    }
}
