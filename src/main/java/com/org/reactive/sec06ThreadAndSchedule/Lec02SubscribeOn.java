package com.org.reactive.sec06ThreadAndSchedule;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class Lec02SubscribeOn {

    public static void main(String[] args) {

        Flux<Object> flux = Flux.create(fluxSink -> {
            printThreadName("Create Method");
            fluxSink.next(1);
        }).doOnNext(i -> printThreadName("Next Method: " + i));

        flux.doFirst(() -> printThreadName("First method 1"))
                //.subscribeOn(Schedulers.parallel())
                .subscribeOn(Schedulers.boundedElastic())
                .doFirst(() -> printThreadName("First method 2"))
                .subscribe(v -> printThreadName("Subscription Method: " + v));

       /* Runnable runnable = () -> flux.doFirst(() -> printThreadName("First method 1"))
                //.subscribeOn(Schedulers.parallel())
                .subscribeOn(Schedulers.boundedElastic())
                .doFirst(() -> printThreadName("First method 2"))
                .subscribe(v -> printThreadName("Subscription Method: " + v));

        for (int i = 0; i < 2; i++) {
            new Thread(runnable).start();
        }*/

        Utils.sleep(5);

    }

    private static void printThreadName(String msg) {
        System.out.println(msg + "\t\t\tThread:\t" + Thread.currentThread().getName());
    }
}
