package com.org.reactive.sec06ThreadAndSchedule;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;

public class Lec01ThreadDemo {

    public static void main(String[] args) {

        Flux<Object> flux = Flux.create(fluxSink -> {
            printThreadName("Create Method");
            fluxSink.next(1);
        }).doOnNext(i -> printThreadName("Next Method: " + i));

        Runnable runnable = () -> flux.subscribe(v -> printThreadName("Subscription Method: " + v));

        for (int i = 0; i < 2; i++) {
            new Thread(runnable).start();
        }

        Utils.sleep(5);

    }

    private static void printThreadName(String msg) {
        System.out.println(msg + "\tThread:\t" + Thread.currentThread().getName());
    }
}
