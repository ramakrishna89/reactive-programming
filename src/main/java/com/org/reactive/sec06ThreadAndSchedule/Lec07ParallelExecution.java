package com.org.reactive.sec06ThreadAndSchedule;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class Lec07ParallelExecution {

    public static void main(String[] args) {


        Flux.range(1, 10)
                // .parallel()
                .parallel(2)
                .runOn(Schedulers.parallel())
                .doOnNext(i -> printThreadName("NEXT"))
                // .sequential()
                .subscribe(i -> printThreadName("SUBS"));

        Utils.sleep(5);
    }

    private static void printThreadName(String msg) {
        System.out.println(msg + "\t\tThread:\t" + Thread.currentThread().getName());
    }
}
