package com.org.reactive.sec06ThreadAndSchedule;

import com.org.reactive.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class Lec05PublishOn {

    public static void main(String[] args) {

        Flux<Object> flux = Flux.create(fluxSink -> {
            printThreadName("Create Method");
            for (int i = 0; i < 20; i++) {
                fluxSink.next(i);
            }
            fluxSink.complete();
        }).doOnNext(i -> printThreadName("Next Method: " + i));

        flux.publishOn(Schedulers.boundedElastic())
                .doOnNext(i -> printThreadName("Nex1 Method: " + i))
                .publishOn(Schedulers.parallel())
                .subscribe(v -> printThreadName("Subs Method: " + v));

        /*Runnable runnable = () -> flux.subscribeOn(Schedulers.boundedElastic())
                .subscribe(v -> printThreadName("Subs Method: " + v));

        for (int i = 0; i < 2; i++) {
            new Thread(runnable).start();
        }*/

        Utils.sleep(5);

    }

    private static void printThreadName(String msg) {
        System.out.println(msg + "\t\tThread:\t" + Thread.currentThread().getName());
    }
}
