package com.org.reactive.test;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class Lec02ErrorTest {

    @Test
    public void test1() {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Object> flux2 = Flux.error(new RuntimeException("SAMPLE ERROR"));
        Flux<Object> concatFlux = Flux.concat(flux1, flux2);

        StepVerifier.create(concatFlux)
                .expectNext(1, 2, 3)
                .verifyError();
    }

    @Test
    public void test2() {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Object> flux2 = Flux.error(new RuntimeException("SAMPLE ERROR"));
        Flux<Object> concatFlux = Flux.concat(flux1, flux2);

        StepVerifier.create(concatFlux)
                .expectNext(1, 2, 3)
                .verifyError(NullPointerException.class);
    }


    @Test
    public void test3() {
        Flux<Integer> flux1 = Flux.just(1, 2, 3);
        Flux<Object> flux2 = Flux.error(new RuntimeException("SAMPLE ERROR"));
        Flux<Object> concatFlux = Flux.concat(flux1, flux2);

        StepVerifier.create(concatFlux)
                .expectNext(1, 2, 3)
                .verifyErrorMessage("SAMPLE ERROR");
    }

}
